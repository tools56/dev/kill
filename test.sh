#!/bin/bash

trap "echo 'sig INT caught' " INT

if [[ "${boolKill}" == "true" ]]; then
   echo "Kill -SIGINT $$"
   /usr/bin/kill
   kill -SIGINT $$
fi
echo "Fin"
exit 0
